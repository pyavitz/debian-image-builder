# ATF
armtrusted_firmware () {
echo Downloading arm trusted firmware.
git clone https://github.com/ARM-software/arm-trusted-firmware.git
cd arm-trusted-firmware
echo
echo Compiling ATF.
export CROSS_COMPILE=aarch64-linux-gnu-
make -j${CORES} PLAT=sun50i_a64 DEBUG=1 bl31 &> /dev/null
cd ..
echo Done.
}

# UBOOT
npineoplus2_uboot () {
echo
echo Downloading u-boot.
wget -cq --show-progress https://gitlab.denx.de/u-boot/u-boot/-/archive/${UBOOT_VERSION}/u-boot-${UBOOT_VERSION}.tar.gz
echo Done.
echo
echo Extracting archive.
tar xf u-boot-${UBOOT_VERSION}.tar.gz
echo Done.
mv -f u-boot-${UBOOT_VERSION} u-boot
cp arm-trusted-firmware/build/sun50i_a64/debug/bl31.bin u-boot/bl31.bin
cp ${DEF}/h5-npineoplus2_defconfig u-boot/configs/h5-npineoplus2_defconfig
cd u-boot
echo
echo Compiling uboot.
export CROSS_COMPILE=aarch64-linux-gnu-
make h5-npineoplus2_defconfig
#make menuconfig
make -j${CORES}
}

pine64_uboot () {
echo
echo Downloading u-boot.
wget -cq --show-progress https://gitlab.denx.de/u-boot/u-boot/-/archive/${UBOOT_VERSION}/u-boot-${UBOOT_VERSION}.tar.gz
echo Done.
echo
echo Extracting archive.
tar xf u-boot-${UBOOT_VERSION}.tar.gz
echo Done.
mv -f u-boot-${UBOOT_VERSION} u-boot
cp arm-trusted-firmware/build/sun50i_a64/debug/bl31.bin u-boot/bl31.bin
cp ${DEF}/a64-pine64-plus_defconfig u-boot/configs/a64-pine64-plus_defconfig
cd u-boot
echo
echo Compiling.
export CROSS_COMPILE=aarch64-linux-gnu-
make a64-pine64-plus_defconfig
#make menuconfig
make -j${CORES}
}

tritium_uboot () {
echo
echo Downloading u-boot.
wget -cq --show-progress https://gitlab.denx.de/u-boot/u-boot/-/archive/${UBOOT_VERSION}/u-boot-${UBOOT_VERSION}.tar.gz
echo Done.
echo
echo Extracting archive.
tar xf u-boot-${UBOOT_VERSION}.tar.gz
echo Done.
mv -f u-boot-${UBOOT_VERSION} u-boot
cp arm-trusted-firmware/build/sun50i_a64/debug/bl31.bin u-boot/bl31.bin
cp ${DEF}/libretech-all-h5-cc_defconfig u-boot/configs/libretech-all-h5-cc_defconfig
cd u-boot
echo
echo Compiling uboot.
export CROSS_COMPILE=aarch64-linux-gnu-
make libretech-all-h5-cc_defconfig
#make menuconfig
make -j${CORES}
}

# BINARY
npineoplus2_binary () {
cp -f u-boot-sunxi-with-spl.bin ../${BINNPI}/
cp -f tools/mkimage ../${BINNPI}/
echo Done.
echo
}

pine64_binary () {
cp -f u-boot-sunxi-with-spl.bin ../${BINPINE}/
cp -f tools/mkimage ../${BINPINE}/
echo Done.
}

tritium_binary () {
cp -f u-boot-sunxi-with-spl.bin ../${BINTRI}/
cp -f tools/mkimage ../${BINTRI}/
echo Done.
echo
}

## KERNEL
# TRITIUM
tritium_patch () {
    echo
    echo Applying patches.
    patch -p1 < ../${PACKAGING}/allwinner-packaging.patch
    for i in ../${ALLWINNER}/*.patch; do patch -p1 < $i; done
    cp -f ../${PACKAGING}/headers-byteshift.patch headers-byteshift.patch
    echo Done.
    echo
}

tritium_defconfig () {
    echo Making builder defconfig.
    cp -f ../${DEF}/tritium_defconfig arch/arm64/configs/
    make ARCH=arm64 tritium_defconfig &> /dev/null
    echo Done.
}

# PINE64
pine64_patch () {
echo
echo Applying patches.
patch -p1 < ../${PACKAGING}/allwinner-packaging.patch
for i in ../${PINE}/cpufreq/*.patch; do patch -p1 < $i; done
for i in ../${PINE}/wifibt/*.patch; do patch -p1 < $i; done
for i in ../${PINE}/misc/*.patch; do patch -p1 < $i; done
cp -f ../${PACKAGING}/headers-byteshift.patch headers-byteshift.patch
echo Done.
echo
}

pine64_defconfig () {
    echo Making builder defconfig.
    cp -f ../${DEF}/pinea64plus_defconfig arch/arm64/configs/
    make ARCH=arm64 pinea64plus_defconfig &> /dev/null
    echo Done.
}

# NANOPI
nanopi_patch () {
echo
echo Applying patches.
patch -p1 < ../${PACKAGING}/allwinner-packaging.patch
for i in ../${ALLWINNER}/*.patch; do patch -p1 < $i; done
cp -f ../${PACKAGING}/headers-byteshift.patch headers-byteshift.patch
echo Done.
echo
}

nanopi_defconfig () {
    echo Making builder defconfig.
    cp -f ../${DEF}/npineoplus2_defconfig arch/arm64/configs/
    make ARCH=arm64 npineoplus2_defconfig &> /dev/null
    echo Done.
}

## STAGE1
tritium_flashbin () {
dd if=binary/tritium/u-boot-sunxi-with-spl.bin of="${IMAGE_LOOP_DEV}" conv=fsync bs=1024 seek=8
}

nanopi_flashbin () {
dd if=binary/nanopi/u-boot-sunxi-with-spl.bin of="${IMAGE_LOOP_DEV}" conv=fsync bs=1024 seek=8
}

pine64_flashbin () {
dd if=binary/pine64/u-boot-sunxi-with-spl.bin of="${IMAGE_LOOP_DEV}" conv=fsync bs=1024 seek=8
}

## STAGE2
tritium_extlinux () {
cd ~
echo
echo Adding extlinux file.
sleep 1s
mkdir -p /boot/extlinux
tee /boot/extlinux/extlinux.conf <<EOF
label kernel
    kernel /boot/Image
    fdt /boot/allwinner/sun50i-h5-libretech-all-h3-cc.dtb
    append  earlyprintk console=ttyS2,115200n8 rw root=PARTUUID=${ROOT_PARTUUID} rootwait rootfstype=ext4 init=/sbin/init
EOF
echo Done
}

nanopi_extlinux () {
cd ~
echo
echo Creating extlinux file.
mkdir -p /boot/extlinux
tee /boot/extlinux/extlinux.conf <<EOF
label kernel
    kernel /boot/Image
    fdt /boot/allwinner/sun50i-h5-nanopi-neo-plus2.dtb
    append  earlyprintk console=ttyS2,115200n8 rw root=PARTUUID=${ROOT_PARTUUID} rootwait rootfstype=ext4 init=/sbin/init
EOF
echo Done
}

pine64_extlinux () {
cd ~
echo
echo Adding extlinux file.
sleep 1s
mkdir -p /boot/extlinux
tee /boot/extlinux/extlinux.conf <<EOF
label /boot/kernel
    kernel /boot/Image
    fdt /boot/allwinner/sun50i-a64-pine64-plus.dtb
    append  earlyprintk console=ttyS2,115200n8 rw root=PARTUUID=${ROOT_PARTUUID} rootwait rootfstype=ext4 init=/sbin/init
EOF
echo Done.
}

tritium_led_triggers () {
echo
echo Creating LED triggers.
tee /usr/local/sbin/led-triggers <<EOF
#!/bin/bash
# led triggers
echo 0 > /sys/class/leds/librecomputer:green:pwr/brightness
echo 1 > /sys/class/leds/librecomputer:blue:status/brightness
sleep 2s
echo 0 > /sys/class/leds/librecomputer:blue:status/brightness
EOF
}

tritium_led_service () {
echo
echo Creating led service.
sleep 1s
tee /etc/systemd/system/leds.service <<EOF
[Unit]
Description=Set LEDs
ConditionPathExists=/usr/local/sbin/led-triggers
[Service]
Type=forking
ExecStart=/usr/local/sbin/led-triggers &>/dev/null
[Install]
WantedBy=multi-user.target
EOF
systemctl enable leds
}

cypress_firmware () {
echo
echo Adding brcm firmware from cypress.
sleep 1s
mkdir cypress
cd cypress
wget -cq --show-progress https://community.cypress.com/servlet/JiveServlet/download/19375-1-53475/cypress-fmac-v5.4.18-2020_0402.zip
unzip -qq cypress-fmac-v5.4.18-2020_0402.zip
tar -xf cypress-firmware-v5.4.18-2020_0402.tar.gz
rm -f firmware/*pcie*
cp -f firmware/* /lib/firmware/brcm/
cd ~
rm -fdr cypress
echo Done.
}

extra_firmware () {
echo
echo Adding extra firmware.
sleep 1s
mkdir -p /lib/firmware/brcm
cd /lib/firmware/brcm
wget -cq --show-progress https://raw.githubusercontent.com/buildroot/buildroot/master/board/friendlyarm/nanopi-neo-plus2/rootfs_overlay/lib/firmware/brcm/brcmfmac43430-sdio.friendlyarm%2Cnanopi-neo-plus2.txt
cd ~
wget -cq --show-progress https://github.com/armbian/firmware/archive/master.tar.gz
tar xf master.tar.gz
cp -R firmware-master/* /lib/firmware
rm -fdr firmware-master master.tar.gz
mv fw-0a5c_21e8.hcd /lib/firmware/brcm/BCM20702A0-0a5c-21e8.hcd
cp /lib/firmware/brcm/BCM20702A0-0a5c-21e8.hcd /lib/firmware/brcm/BCM20702A1-0a5c-21e8.hcd
chown root:root /lib/firmware/brcm/BCM20702A1-0a5c-21e8.hcd
chown root:root /lib/firmware/brcm/BCM20702A0-0a5c-21e8.hcd
echo Done.
}

pine64_bt_helper () {
echo
echo Creating bluetooth helper.
tee /usr/local/sbin/start-bluetooth <<EOF
#!/bin/bash
echo 1 > /sys/class/rfkill/rfkill0/state
sleep 1s
rtk_hciattach /dev/ttyS1 rtk_h5
sleep 2s
hciconfig hci0 up
EOF
}

pine64_bt_install () {
echo
echo Installing bluetooth helper.
sleep 1s
git clone https://github.com/lwfinger/rtl8723bs_bt.git
cd rtl8723bs_bt
make
make install
mv rtk_hciattach /usr/local/bin/
cd ~
rm -fdr rtl8723bs_bt
echo Done.
}

pine64_wifi_bt () {
echo
echo Bluetooth and wifi help.
sleep 1s
tee /etc/modules-load.d/pine64.conf <<EOF
r8723bs
hci_urt

EOF

tee /etc/modprobe.d/wifi-pwrmgnt.conf <<EOF
options r8723bs rtw_power_mgnt=0

EOF
}

pine64_bt_service () {
echo
echo Creating bluetooth service.
sleep 1s
tee /etc/systemd/system/bluetooth-module.service <<EOF
[Unit]
Description=Enable Bluetooth
ConditionPathExists=/usr/local/sbin/start-bluetooth
[Service]
Type=forking
ExecStart=/usr/local/sbin/start-bluetooth &>/dev/null
[Install]
WantedBy=multi-user.target
EOF
systemctl enable bluetooth-module
}
